$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
      interval: 2000
    });

    // cuando se muestre el modal, el objeto e es el evento
    // on siempre se usa para escribir los eventos
    $("#contacto").on("show.bs.modal", function (e) {
      console.log("el modal se estra mostrando")
      $("#contactoBtn").removeClass("btn-outline-success")
      $("#contactoBtn").addClass("btn-primary")
      $("#contactoBtn").prop("disabled", true)
    })

    $("#contacto").on("shown.bs.modal", function (e) {
      console.log("el modal se mostro")
    })

    $("#contacto").on("hide.bs.modal", function (e) {
      console.log("el modal se estra oculta")
    })

    $("#contacto").on("hidden.bs.modal", function (e) {
      console.log("el modal se oculto")
      $("#contactoBtn").prop("disabled", false)
    })

  });